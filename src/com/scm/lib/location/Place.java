package com.scm.lib.location;

/**
 * Clase para manipular las devoluciones de  
 * @author Domin
 *
 */
public class Place {
	
	private double latitude;
	private double longitude;
	private String namePlace;
	
	public Place(double latitude, double longitude, String namePlace){
		this.latitude = latitude;
		this.longitude = longitude;
		this.namePlace = namePlace;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public String getNamePlace() {
		return namePlace;
	}

	public void setNamePlace(String namePlace) {
		this.namePlace = namePlace;
	}
}
