package com.scm.practica3.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;


public class DummyF extends Fragment implements View.OnLongClickListener, View.OnClickListener {

        public static final String ARG_SECTION_NUMBER = "section_number";
        private Bundle args;
        
       
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
        	
        	ViewGroup.LayoutParams layoutparams= new ViewGroup.LayoutParams(
        			ViewGroup.LayoutParams.MATCH_PARENT,
        			ViewGroup.LayoutParams.MATCH_PARENT);
        	
        	ViewGroup resView = initLayout(layoutparams);        	
        	        	
        	args = getArguments();
        	
        	resView.setOnClickListener(this);
        	resView.setOnLongClickListener(this);
            return resView;
        }
        
        //Inicializa y devuelve un layaout Relativo
        protected ViewGroup initLayout(ViewGroup.LayoutParams params){
        	FrameLayout layout = new FrameLayout(getActivity().getApplicationContext());
        	if (params!=null)
        		layout.setLayoutParams(params);
        	layout=this.insertInLayout(layout);
        	layout.setBackgroundColor(Color.YELLOW);
        	return layout;
        }
        
        
        protected FrameLayout insertInLayout(FrameLayout layout){        	
        	
        	FrameLayout.LayoutParams params= new FrameLayout.LayoutParams(
    			FrameLayout.LayoutParams.WRAP_CONTENT,
    			FrameLayout.LayoutParams.WRAP_CONTENT,
    			Gravity.CENTER);
        	
        	
        	TextView resView;	        	
        	//Si queremos selecionar un layaout externo (XML)
            //resView = inflater.inflate(R.layout.fragment_section_dummy, container, false);
            
//	            ((TextView) resView.findViewById(android.R.id.text1)).setText(
//	                    "Dummy section num "+args.getInt(ARG_SECTION_NUMBER));	            
//	            TextView resView = new TextView(container.getContext());
            
            
            resView = new TextView(getActivity().getApplicationContext());
            if (args!=null)
            	resView.setText("Dummy section num "+args.getInt(ARG_SECTION_NUMBER));
            else
            	resView.setText("Dummy section");
            resView.setTextColor(Color.MAGENTA);
            resView.setBackgroundColor(Color.BLUE);
            resView.setTextSize(25);
            
            resView.setLayoutParams(params);            
            layout.addView(resView);
            //layout.addView(resView, params);//hace lo mismo que las 2 ultimas instruciones        	
        	return layout;
        }

		@Override
		public boolean onLongClick(View arg0) {
			Log.d("LONGCLICK", "se ha dado un click longo en el dummy");
			//mapView.setSelected(true);
				//XXX: Toast de prueba del click longo en el mapa
				Context context = getActivity().getApplicationContext();
				CharSequence text = "Checking Long Press";
				int duration = Toast.LENGTH_SHORT;
				Toast.makeText(context, text, duration).show();	
			return true;
		}

		@Override
		public void onClick(View v) {
			Log.d("LONGCLICK", "se ha dado un click peque�o en el dummy");
			//mapView.setSelected(true);
				//XXX: Toast de prueba del click longo en el mapa
				Context context = getActivity().getApplicationContext();
				CharSequence text = "Checking click Press";
				int duration = Toast.LENGTH_SHORT;
				Toast.makeText(context, text, duration).show();
		}
}
