package com.scm.practica3.fragments;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.location.LocationListener;
import com.scm.lib.location.LocationGeocoderClient;
import com.scm.lib.location.LocationGeocoderClient.AddressNotifyObserver;
import com.scm.lib.location.Place;
import com.scm.lib.location.res.LocationUtils;
import com.scm.practica3.FunctionsJSON;
import com.scm.practica3.R;


public class InverseGeolocation extends GeolocationAbstract implements View.OnClickListener, LocationGeocoderClient.AddressNotifyObserver, LocationListener{
	
	private static final String TAG = "InverseGeolocation";
	
	// Para usar en las peticiones de direcciones y poner el marcador en el mapa
	private Location mAddressLoc;
	
	//callback reference
	private InverseGeolocationCallbacks mCallbackActivity;	
	
	public interface InverseGeolocationCallbacks{
		public LocationGeocoderClient getLocationGeocoderClient();
		public boolean getPeriodicUpdatesState();
		public void getAddressAsync(ProgressBar progressbar, AddressNotifyObserver addressOb);
		public int getGeolocationMethod();
		public void easyShareAddress(String addr);
	} 
	
	
	private TextView mUpdatesIndicatorTextView;
	private TextView mLatitudeTextView;
	private TextView mLongitudeTextView;
	private TextView mAddressTextView;
	private ProgressBar mProgress;
	private MapListenerWrapper mMapaWrapper;
	
	@Override
	public void onAttach(Activity activity) {		
		super.onAttach(activity);
		Log.d(TAG,TAG+" onAttach()");
		try{
			mCallbackActivity = (InverseGeolocationCallbacks) activity;
	    }catch(ClassCastException ex){
	        Log.e("InverseGeolocation", "El Activity must implement the interface InverseGeolocation.InverseGeolocationCallbacks");
	    }		
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG,TAG+" onCreateView()");
		View view = inflater.inflate(R.layout.inverse_geolocation, container, false);
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(TAG,TAG+" onActivityCreated()");
		super.onActivityCreated(savedInstanceState);		
		
		Button locate;
		locate = (Button) getView().findViewById(R.id.button1);
		locate.setOnClickListener(this);	
		
		locate = (Button) getView().findViewById(R.id.button2);
		locate.setOnClickListener(this);
		
		mUpdatesIndicatorTextView = (TextView) getView().findViewById(R.id.updatesIndicator); 
		mLatitudeTextView = (TextView) getView().findViewById(R.id.latitudeTextView);
		mLongitudeTextView = (TextView) getView().findViewById(R.id.longitudeTextView);
		mAddressTextView = (TextView) getView().findViewById(R.id.addressTextView);		
		mProgress = (ProgressBar) getView().findViewById(R.id.progressBar2);
		
		mMapaWrapper=new MapListenerWrapper();
		Bundle bundle = new Bundle();
		bundle.putInt(MapListenerWrapper.MAP_ID, R.id.map1);
		//bundle.putInt(MapListenerWrapper.MAP_ID, R.id.container1);
		mMapaWrapper.setArguments(bundle);		
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
		transaction.replace(R.id.container1, mMapaWrapper);
		//transaction.add(R.id.container1, fragment);
		transaction.commit();
	}
	
	@Override
	public void onResume() {	
		super.onResume();
		Log.d(TAG,TAG+" onResume()");
		// Actualizamos el texView que indica el estado de las actualizaciones periodicas		
		setUpdateIndicatorState(mCallbackActivity.getPeriodicUpdatesState());
		//Comprobamos como est� el estado de las actualizaciones periodicas de posicion
		if (mCallbackActivity.getPeriodicUpdatesState())//de estar activas, las arrancamos
			mCallbackActivity.getLocationGeocoderClient().startPeriodicUpdates(this);
	}
	
	@Override
	public void onPause() {
		Log.d(TAG,TAG+" onPause()");
		//Comprobamos como est� el estado de las actualizaciones periodicas de posicion
		if (mCallbackActivity.getPeriodicUpdatesState())//de estar activadas, las paramos
			mCallbackActivity.getLocationGeocoderClient().stopPeriodicUpdates(this);
		super.onPause();
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button1:
			//Obtener los valores de latitud y longitud
			mLatitudeTextView.setText(
					mCallbackActivity.getLocationGeocoderClient().getLocationLatitude());
			mLongitudeTextView.setText(
					mCallbackActivity.getLocationGeocoderClient().getLocationLongitude());
				
			break;
		default://R.id.button2
			if (!checkNetwork())
				break;
			if (mCallbackActivity.getGeolocationMethod()==1){//Geolocalizaci�n Inversa con peticiones HTTP
				Log.d(TAG,TAG+" Using Inverse Geocode method HTTP");
				new RequestAdressHttpTask(getActivity().getBaseContext()).execute();
			}				
			else{//Geolocalizaci�n Inversa con Geocoder
				Log.d(TAG,TAG+" Using Inverse Geocode method Geocoder");
				//Obtener la direcci�n a partir de los valores de latitud y longitud
				mCallbackActivity.getAddressAsync(mProgress, this);
				//Actualizar las coordenadas del mapa
				mAddressLoc = mCallbackActivity.getLocationGeocoderClient().getLocation();
				mMapaWrapper.setPosition(mAddressLoc.getLatitude(),mAddressLoc.getLongitude(),15f);				
			}
			break;			
		}		
	}

	private void setUpdateIndicatorState(boolean state){
		if (state)
			mUpdatesIndicatorTextView.setText(Html.fromHtml(getString(R.string.periodic_updates_enable)));
		else
			mUpdatesIndicatorTextView.setText(Html.fromHtml(getString(R.string.periodic_updates_disable)));
	}	

	/***** CallBacks Observadoras *****/

	//Se notifica la resoluci�n de la direcci�n ped�da asincronamente en background. 
	@Override
	public void addressNotify(String address) {
		mAddressTextView.setText(address);
		mMapaWrapper.setMarker(mAddressLoc.getLatitude(),mAddressLoc.getLongitude(),address);
		mCallbackActivity.easyShareAddress(address);
	}

	//Se notifica el cambio de localizacion
	@Override
	public void onLocationChanged(Location location) {
		//a�adimos una A de Automatico solo para diferenciar cuando pulsamos a Localizar 
		//y estan las periodic updates activas		
		mLatitudeTextView.setText(LocationUtils.getLat(this.getActivity(), location)+" A");
		mLongitudeTextView.setText(LocationUtils.getLng(this.getActivity(), location)+" A");		
	}
	
	
	private class RequestAdressHttpTask extends AsyncTask <Void, Void, Place> {
		
		private Context context;
		
		public RequestAdressHttpTask(Context context) {
			this.context=context;
		}	
		
    	protected void onPreExecute() {
    		mProgress.setIndeterminate(true);    		
    	}
		
		protected Place doInBackground(Void... params) {
			Location loc=mCallbackActivity.getLocationGeocoderClient().getLocation();
			String url = context.getString(R.string.inverseGeolocationHTTPrequest,
					String.valueOf(loc.getLatitude()),String.valueOf(loc.getLongitude()));
			Log.d(TAG,TAG+" HTTP_URL: "+url);
        	JSONObject jsonObj = FunctionsJSON.getJSONObjectFromUrl(url);        	
        	
            return FunctionsJSON.getPlaceFromJSONObject(jsonObj);
		}
		
		@Override
        protected void onPostExecute(Place result) {
			mAddressTextView.setText(result.getNamePlace());
        	mProgress.setIndeterminate(false);
        	mMapaWrapper.setPosition(result.getLatitude(),result.getLongitude(),15f);
        	mMapaWrapper.setMarker(result.getLatitude(),result.getLongitude(),result.getNamePlace());
        	mCallbackActivity.easyShareAddress(result.getNamePlace());
        }		
	}
}
