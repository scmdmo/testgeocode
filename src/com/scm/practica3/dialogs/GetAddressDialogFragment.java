package com.scm.practica3.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;

import com.scm.practica3.R;
import com.scm.practica3.fragments.Geolocation;

public class GetAddressDialogFragment extends DialogFragment{
		
	private GetAddressDialogListener activityListener;
		
	public interface GetAddressDialogListener {
        public void onDialogGetAddresClick(DialogFragment dialogfragment, Geolocation parentdialogFragment);        
    }    
    
	private Geolocation parentFragment;
	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {          
            activityListener = (GetAddressDialogListener) activity;
        } catch (ClassCastException e) {            
            throw new ClassCastException(activity.toString()
                    + " must implement GetAddressDialogFragment");
        }
        parentFragment = (Geolocation) this.getTargetFragment();
    }
    
        
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.get_address, null));
        
        builder.setTitle(R.string.diag_GetAddress_title);
        
        builder.setPositiveButton(R.string.diag_GetAddress_ok, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   
                	   activityListener.onDialogGetAddresClick(GetAddressDialogFragment.this,parentFragment);                	   
                   }
        });
        //no necesitamos hacer nada para el boton cancelar
        builder.setNegativeButton(R.string.diag_GetAddress_cancel, null);
        
        //XXX: borrar esto si no fallos una vez probado
//        builder.setNegativeButton(R.string.diag_mapTrigger_buton_cancel, new DialogInterface.OnClickListener() {
//                   public void onClick(DialogInterface dialog, int id) {
//                       // User cancelled the dialog
//                   }
//        });
        
        return builder.create();
    }

}
