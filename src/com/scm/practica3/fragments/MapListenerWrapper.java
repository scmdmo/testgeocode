package com.scm.practica3.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.scm.practica3.R;
import com.scm.practica3.dialogs.MapTriggerDialogFragment;

public class MapListenerWrapper extends Fragment implements GoogleMap.OnMapLongClickListener{

	private int  map_id;
	private LatLng mLatLng;
	private String mlabel="";
	
	private com.google.android.gms.maps.SupportMapFragment mpfr;
	private GoogleMap map;
	
	public static String MAP_ID="MAP_ID";
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {	
		
		map_id=this.getArguments().getInt(MapListenerWrapper.MAP_ID, R.id.map1);

		
		ViewGroup.LayoutParams layoutparams= new ViewGroup.LayoutParams(
    			ViewGroup.LayoutParams.MATCH_PARENT,
    			ViewGroup.LayoutParams.MATCH_PARENT);
		
		FrameLayout layout = new FrameLayout(getActivity().getApplicationContext());		
		layout.setLayoutParams(layoutparams);
		//this.container.setId(View.generateViewId());//Se podria usar esta funci�n pero nos limita a API17
		layout.setId(map_id);
		Log.d("P3-SET_ID",""+map_id); // EXCEDENT: debug
				
		mLatLng=new LatLng(42.8782132,-8.5448445);//latitud longitud por defecto(santiago)
		GoogleMapOptions options = new GoogleMapOptions();
		options.camera(new CameraPosition(mLatLng,8,30,0))//(LatLng target, float zoom, float tilt, float bearing)
			.mapType(GoogleMap.MAP_TYPE_NORMAL)
			.compassEnabled(true)
			.rotateGesturesEnabled(true)
	    	.scrollGesturesEnabled(false)
	    	.tiltGesturesEnabled(true)
	    	.zoomControlsEnabled(true)
	    	.zoomGesturesEnabled(false);			
		
		mpfr = com.google.android.gms.maps.SupportMapFragment.newInstance(options);				
		
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();		
		transaction.replace(map_id, mpfr);
		transaction.commit();	
	
		return layout;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);				
		
		//map = mapfragment.getMap();
		
		//Log.d("P3-GOOGLEMAP",map==null?"map==null":"map!=null");
		
		//map.setOnMapLongClickListener(this);
	}
	
	
	@Override
	public void onStart() {		
		super.onStart();
		map = mpfr.getMap();		
		Log.d("P3-GOOGLEMAP",map==null?"map==null":"map!=null"); // EXCEDENT: debug
		map.setOnMapLongClickListener(this);
	}
	
	@Override
	public void onResume() {		
		super.onResume();
		map.clear();
		
	}
	
	public void setMarker(double latitude, double longitud,String title){
		mlabel=title;
		mLatLng=new LatLng(latitude,longitud);
		map.addMarker(new MarkerOptions()
						.position(mLatLng)
						.title(title));
	}
	
	public void setPosition(double latitude, double longitud, float zoom){
		LatLng latLng = new LatLng(latitude,longitud);
		map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
	}
	
	public void setPosition(double latitude, double longitud){
		LatLng latLng = new LatLng(latitude,longitud);
		//movimiento animado
		map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
		//movimiento instantaneo
		//map.moveCamera(CameraUpdateFactory.newLatLng(latLng));		
	}

	
	@Override
	public void onMapLongClick(LatLng arg0) {
		Log.d("MapListenerWrapper", "se ha dado un click largo en el mapa \"OnMapLongClick\"");
		
		//hacemos vibrar el dispositivo al realizar click longo en el mapa
		Context context = getActivity().getApplicationContext();				
		Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(250l);
		//vibrator.cancel();
		
	    DialogFragment newFragment = new MapTriggerDialogFragment();
	    //A�adimos los metadatos para lanzar el Google Maps
	    Bundle bund = new Bundle();
	    bund.putParcelable(MapTriggerDialogFragment.LATLNG, mLatLng);  
	    bund.putString(MapTriggerDialogFragment.MAP_LABEL, mlabel);
	    //TODO: podemos poner tambi�n el zoom del mapa, ya hice un tag para el en DialogFragment
	    newFragment.setArguments(bund);
	    newFragment.show(getFragmentManager(), "google_maps");				
	}

//----------------------------------2 version //EXCEDENT versiones anteriores

//public class MapListenerWrapper extends Fragment implements GoogleMap.OnMapLongClickListener{
//	
//	//private MapView  mapView;
//	private int  map_id;
//	private com.google.android.gms.maps.SupportMapFragment mpfr;
//	private GoogleMap map;
//	
//	public static String MAP_ID="MAP_ID";
//	
//	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		
//		map_id=this.getArguments().getInt(MapListenerWrapper.MAP_ID, R.id.map1);
//		
//		ViewGroup.LayoutParams layoutparams= new ViewGroup.LayoutParams(
//    			ViewGroup.LayoutParams.MATCH_PARENT,
//    			ViewGroup.LayoutParams.MATCH_PARENT);
//		
//		FrameLayout layout = new FrameLayout(getActivity().getApplicationContext());		
//		layout.setLayoutParams(layoutparams);
//		//this.container.setId(View.generateViewId());//Se podria usar esta funci�n pero nos limita a API17
//		layout.setId(map_id);
//		Log.d("P3-SET_ID",""+map_id);
//		return layout;
//	}
//	
//	
//	@Override
//	public void onActivityCreated(Bundle savedInstanceState) {
//		super.onActivityCreated(savedInstanceState);		
//		
//		GoogleMapOptions options = new GoogleMapOptions();
//		options.camera(new CameraPosition(new LatLng(42.8782132,-8.5448445),8,30,0))//(LatLng target, float zoom, float tilt, float bearing)
//			.mapType(GoogleMap.MAP_TYPE_NORMAL)
//			.compassEnabled(true)
//			.rotateGesturesEnabled(true)
//	    	.scrollGesturesEnabled(false)
//	    	.tiltGesturesEnabled(true)
//	    	.zoomControlsEnabled(true)
//	    	.zoomGesturesEnabled(false);			
//		
//		com.google.android.gms.maps.SupportMapFragment mapfragment =com.google.android.gms.maps.SupportMapFragment.newInstance(options);
//		mpfr=mapfragment;
//		
//		
//		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();		
//		transaction.replace(map_id, mapfragment); //XXX: No estoy seguro si este identificador dara problemas con el del layaout del otro fragment (parece que no)
//		transaction.commit();
//		
//		//map = mapfragment.getMap();
//		
//		//Log.d("P3-GOOGLEMAP",map==null?"map==null":"map!=null");
//		
//		//map.setOnMapLongClickListener(this);
//	}
//	
//	
//	@Override
//	public void onStart() {		
//		super.onStart();
//		map = mpfr.getMap();		
//		Log.d("P3-GOOGLEMAP",map==null?"map==null":"map!=null");
//		map.setOnMapLongClickListener(this);
//	}
//	
//	@Override
//	public void onResume() {		
//		super.onResume();
//		map.clear();
//		
//	}
//	
//	public void updateOptions(){
//		
//	}
//	
//	public void setPosition(){
//		
//	}
//
//	
//	@Override
//	public void onMapLongClick(LatLng arg0) {
//		Log.d("MapListenerWrapper", "se ha dado un click largo en el mapa \"OnMapLongClick\"");		
//	    DialogFragment newFragment = new MapTriggerDialogFragment();
//	    newFragment.show(getFragmentManager(), "google_maps");				
//	}
	
	
//_----------------------------------primera version	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		
//		int mapResource=R.layout.map_layout;		
//		if(this.getArguments()!=null){
//			Log.d("DEBUG-MAP", "getarguments 1");
//			mapResource=this.getArguments().getInt(MapListenerWrapper.MAP_LAYOUT, R.layout.map_layout);
//		}
//			
//		
//		
//		mapView = (View) inflater.inflate(R.layout.map_layout, container, false);		
//		//mapView = (View) inflater.inflate(mapResource, container, false);
//		
//		//int mapfrid=R.id.mapFragment;
//		if(this.getArguments()!=null){
//			Log.d("DEBUG-MAP", "getarguments 2");
//		//	mapfrid=R.id.mapFragment2;
//		}
//		
//				
//		SupportMapFragment mapfr;
//		mapfr = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mapFragment);
//		//mapfr = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(mapfrid);
//		try{
//			map = mapfr.getMap();
//		} catch (NullPointerException e) {			
//            throw new NullPointerException("Unfortunately SupportMapFragment.findFragmentById(R.id.mapFragment)"
//                    + " returned null, didn't find R.id.mapFragment in the context");
//        }
//		
//		map.setOnMapLongClickListener(this);
//		
//		return mapView;
//	}	
	
}
