package com.scm.practica3.fragments;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.scm.lib.location.Place;
import com.scm.lib.location.res.LocationUtils;
import com.scm.practica3.FunctionsJSON;
import com.scm.practica3.R;
import com.scm.practica3.dialogs.GetAddressDialogFragment;


public class Geolocation extends GeolocationAbstract implements View.OnClickListener {
	
	private static final String TAG = "Geolocation";
	
	//callback reference
	private GeolocationCallbacks mCallbackActivity;	
	
	public interface GeolocationCallbacks{
		public String getAddressGeolocation();
	}	
	
	//dirrecion introducida por el usuario
	String mAddress;
	
	//textviews del fragment
	private TextView mAddressTextView;	
	private TextView mLatitudeTextView;
	private TextView mLongitudeTextView;
	//progressBar
	private ProgressBar mProgress;
	//map wrapper
	private MapListenerWrapper mMapaWrapper;
	
	
	@Override
	public void onAttach(Activity activity) {		
		super.onAttach(activity);
		Log.d(TAG,TAG+" onAttach()");
		try{
			mCallbackActivity = (GeolocationCallbacks) activity;
	    }catch(ClassCastException ex){
	        Log.e("Geolocation", "El Activity must implement the interface Geolocation.GeolocationCallbacks");
	    }		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		Log.d(TAG,TAG+" onCreate()");
		this.mAddress=null;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG,TAG+" onCreateView()");
		View view = inflater.inflate(R.layout.geolocation, container, false);	
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {	
		super.onActivityCreated(savedInstanceState);
		Log.d(TAG,TAG+" onActivityCreated()");
		
		mAddressTextView = (TextView) getView().findViewById(R.id.textView9);
		mLatitudeTextView =  (TextView) getView().findViewById(R.id.textView4);
		mLongitudeTextView = (TextView) getView().findViewById(R.id.textView6);
		mProgress = (ProgressBar) getView().findViewById(R.id.progressBar1);
		
		ImageButton edit;
		edit = (ImageButton) getView().findViewById(R.id.imageButton1);		
		edit.setOnClickListener(this);
		
		Button locate;
		locate = (Button) getView().findViewById(R.id.button1);
		locate.setOnClickListener(this);
		
		mMapaWrapper=new MapListenerWrapper();
		Bundle bundle = new Bundle();
		bundle.putInt(MapListenerWrapper.MAP_ID, R.id.map2);
		//bundle.putInt(MapListenerWrapper.MAP_ID, R.id.container2);
		mMapaWrapper.setArguments(bundle);
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
		transaction.replace(R.id.container2, mMapaWrapper); //XXX: No estoy seguro si este identificador dara problemas con el del layaout del otro fragment (parece que no)
		//transaction.add(R.id.container2, fragment); //XXX: No estoy seguro si este identificador dara problemas con el del layaout del otro fragment (parece que no)
		transaction.commit();		
	}
	
	@Override
	public void onResume() {	
		super.onResume();
		Log.d(TAG,TAG+" onResume()");
		mAddress=mCallbackActivity.getAddressGeolocation();
		if(mAddress!=null)
			mAddressTextView.setText(mAddress);
	}
	
	@Override
	public void onClick(View v) {				
		switch (v.getId()) {
			case R.id.imageButton1:
				Log.d("Geolocation", "se ha dado un click en el imageButton1 \"OnClick\"");	//XXX: Debug Log.d	
			    DialogFragment newFragment = new GetAddressDialogFragment();
			    newFragment.setTargetFragment(this,0);
			    newFragment.show(getFragmentManager(), "edit_button");		
				break;
			default://R.id.button1 (el boton de localizar)
				//Ejecutar una geolocalizacion, obtener los valores da latitud y longitud apartir de la direccion escrita
				if (mAddress==null || (mAddress!=null && mAddress.isEmpty())){
					Toast.makeText(getActivity().getBaseContext(), 
							this.getString(R.string.introAddress), 
							Toast.LENGTH_LONG).show();
					break;
				}
				if (checkNetwork())//si hemos escrito alguna direccion
					resolveAdress();
				break;
		}	

	}
	
	public void setAddress(String addr){						
		mAddressTextView.setText(addr);	
		this.mAddress=addr;
	}
	
	private void resolveAdress(){
		new RequestCoordinatesHttpTask(getActivity().getBaseContext()).execute(mAddress);
	}
	
private class RequestCoordinatesHttpTask extends AsyncTask <String, Void, Place> {
    	
		private Context context;
		
		public RequestCoordinatesHttpTask(Context context) {
			this.context=context;
		}	
		
    	protected void onPreExecute() {
    		mProgress.setIndeterminate(true);    		
    	}
    	
        @Override
        protected Place doInBackground(String... params) {        	
			String url = context.getString(R.string.geolocationHTTPrequest, 
					params[0].replace(' ','+').trim());
			Log.d(TAG,TAG+" HTTP_URL: "+url);
        	JSONObject jsonObj = FunctionsJSON.getJSONObjectFromUrl(url);        	
        	
            return FunctionsJSON.getPlaceFromJSONObject(jsonObj);
        }

        @Override
        protected void onPostExecute(Place result) {        	
            mLatitudeTextView.setText(LocationUtils.getLat(context, result));
            mLongitudeTextView.setText(LocationUtils.getLng(context, result));
        	mProgress.setIndeterminate(false);
        	mMapaWrapper.setPosition(result.getLatitude(),result.getLongitude(),15f);
        	mMapaWrapper.setMarker(result.getLatitude(),result.getLongitude(),result.getNamePlace());
        }        
    }
	
}
