package com.scm.lib.location;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.scm.lib.location.dialogs.ErrorDialogFragment;
import com.scm.lib.location.res.LocationUtils;
import com.scm.practica3.R;

public class LocationGeocoderClient implements GoogleApiClient.ConnectionCallbacks,
								 GoogleApiClient.OnConnectionFailedListener {

	@SuppressWarnings("unused")
	private static final String TAG = "LocationGeocoderClient";
	
	//***********Interfaces Propias***************************//	
	
	/**
	 * Interfaz que deben extender las clases que desen recibir una direcci�n 
	 * @author Domin	 
	 */
	public interface AddressNotifyObserver {
        public void addressNotify(String address);        
    }
	
	
	//Activity asociada al servicio de geolocalizacion
	private FragmentActivity mParentactivity;
	
	//Almacena la instanciacion actual del cliente de localizacion
    private GoogleApiClient mGoogleApiClient;
    

    //Peticion para conectar a los servicios de localización (Actualizacions automaticas)
    private LocationRequest mLocationRequest;
    
    //Peticion para conectar las actualizaciones automaticas en el onConnect 
    private boolean mRequesStartUpdates; 
    
    //Objeto que recibirá las actualizaciones de localización.
	private LocationListener mLocationListener;
	
	/**
	 * Creamos un LocationGeocoderClient nuevo donde el mismo objeto implementar� las interfaces  
	 * {@link GoogleApiClient.ConnectionCallbacks} y {@link GoogleApiClient.OnConnectionFailedListener}.
	 * Puede extenderse el objeto para definir el comportamiento de los callbacks.
	 * @param parentactivity  FragmentActivity que tenga el contexto asociado.
	 */
	public LocationGeocoderClient(FragmentActivity parentactivity) {
		this.mParentactivity=parentactivity;	
		
		this.mGoogleApiClient = new GoogleApiClient.Builder(parentactivity.getApplicationContext())
        .addApi(LocationServices.API)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .build();		
		
		initRequestObjet();
	}
	
	/**
	 * Crea un nuevo LocationGeocoderClient
	 * @param parentactivity  FragmentActivity que tenga el contexto asociado.
	 * @param connectionCallbacks objeto donde se recibir�n los callbacks definidos en {@link GoogleApiClient.ConnectionCallbacks} 
	 * @param connectionFailedListener objeto donde se recibir�n los callbacks definidos en {@link GoogleApiClient.OnConnectionFailedListener}.
	 */
	public LocationGeocoderClient(FragmentActivity parentactivity, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener connectionFailedListener ) {
		this.mParentactivity=parentactivity;		
		
		this.mGoogleApiClient = new GoogleApiClient.Builder(parentactivity.getApplicationContext())
        .addApi(LocationServices.API)
        .addConnectionCallbacks(connectionCallbacks)
        .addOnConnectionFailedListener(connectionFailedListener)
        .build();		
		
		initRequestObjet();
	}
	
	
	//***********callbacks ConnectionCallbacks************//

	/**
     * Llamado por los servicios de localizaci�n cuando la solicitud
     * de conexi�n del cliente se realiza correctamente. En este
     * punto, se puede solicitar la ubicaci�n actual o iniciar
     * las actualizaciones peri�dicas
     */
	@Override
	public void onConnected(Bundle bundle) {
		Toast.makeText(this.mParentactivity, 
				this.mParentactivity.getString(R.string.connected),
				Toast.LENGTH_SHORT).show();
		// si se a intentado conectar las actualizaciones anteriormente pero
		//aun se estaba realizando la tarea de conexi�n, se reintenta ahora que ya esta conectado
		if(mRequesStartUpdates){ 
			LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, mLocationListener);
			mRequesStartUpdates = false; //ponemos la peticion a false
		}
	}
	
	/**
	 * Llamado por los Servicios de ubicaci�n si la conexi�n
	 * con el cliente de ubicaci�n cae debido a un error.
     */
	@Override
	public void onConnectionSuspended(int arg0) {
		Toast.makeText(this.mParentactivity, 
				this.mParentactivity.getString(R.string.disconnected),
				Toast.LENGTH_SHORT).show();
	}
	 
	
	//***********callback OnConnectionFailedListener************//
	
	/**
     * Los servicios de Google Play pueden resolver algunos errores
     * que detecte. Si el error tiene una soluci�n, intenta enviar 
     * un Intent para iniciar una Google Play services activity 
     * que pueda resolver el error.
     * Se aconseja <b>enf�ticamente</b> extender el metodo onActivityResult()
     * de la activity que utilice {@link LocationGeocoderClient} para determinar
     * el resultado. Ver el metodo OnActivityResult de esta clase. 
     */
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
        
        if (connectionResult.hasResolution()) {
            try {
            	// Inicia una Activity que intenta resolver el error
                connectionResult.startResolutionForResult(mParentactivity,
                        LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);
              // Se lanza si Google Play services cancela el intent pendiente
            } catch (IntentSender.SendIntentException e) {
            	// Log the error
                e.printStackTrace();
            }
        } else {
        	// Si no existe una resoluci�n disponible, presenta un dialog al usuario con el error.
            showErrorDialog(connectionResult.getErrorCode());
        }		
	}
	
	
	/**
	 * Maneja los resultados devueltos despues de haber invocado startResolutionForResult().
	 * En particular, el metodo onConnectionFailed() invoca esta llamada para lanzar una Activity que
	 * maneja los problemas del servicio de Google Play. El restultado de esa llamada ser� devuelta en el
	 * onActivityResult() asociado a la activity que creo este objeto.
	 * 
	 * Es necesario que se llame desde esa activity a este m�todo para determinar el resultado.
	 * Si se necesitan tratar mas regresos de activities, comprobar el requestCode con  
	 * 	LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST antes de invocar este m�todo desde el
	 * onActivityResult() del activity.   
	 * @param requestCode  codigo de petici�n
	 * @param resultCode  codigo de resultado 
	 * @param intent intent procedente de la activity evaluada
	 */	    
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

    	// Elegimos que hacer basandonos en el codigo de peticion (requestCode)
        switch (requestCode) {

        	// Si el codigo de petici�n es igual al c�digo enviado en onConnectionFailed
            case LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST :

                switch (resultCode) {
                	// Si Google Play services resolvio el problema
                    case Activity.RESULT_OK:

                        // Log el resultado
                        Log.d(LocationUtils.APPTAG, this.mParentactivity.getString(R.string.resolved));
                        Log.d(LocationUtils.APPTAG, this.mParentactivity.getString(R.string.connected));

                        // Mostramos el resultado	                        
                        Toast.makeText(this.mParentactivity, 
            					this.mParentactivity.getString(R.string.resolved),
            					Toast.LENGTH_SHORT).show();
                        Toast.makeText(this.mParentactivity, 
            					this.mParentactivity.getString(R.string.connected),
            					Toast.LENGTH_SHORT).show();
                    break;

                    // Si cualquier otro resultado es devuelto por Google Play services
                    default:
                        // Log el resultado
                        Log.d(LocationUtils.APPTAG, this.mParentactivity.getString(R.string.no_resolution));

                        // Mostramos el resultado
                        Toast.makeText(this.mParentactivity, 
            					this.mParentactivity.getString(R.string.disconnected),
            					Toast.LENGTH_SHORT).show();
                        Toast.makeText(this.mParentactivity, 
            					this.mParentactivity.getString(R.string.no_resolution),
            					Toast.LENGTH_SHORT).show();
                    break;
                }

            // Si cualquier otro codigo de petici�n fue recibido
            default:
               // Informar de que esta actividad recibi� una requestCode desconocido
               Log.d(LocationUtils.APPTAG,
            		   this.mParentactivity.getString(R.string.unknown_activity_request_code, requestCode));

               break;
        }
    }
	
	
	//***********internal methods ******************************//

    /**
	 * Inicializa el objeto de petici�n de localizaci�n donde se establecer�n los par�metros 
	 * para las actualizaciones peri�dicas de localizaci�n
	 */
	private final void initRequestObjet(){
		//Crea un nuevo objeto de par�metros de localizaci�n global
        mLocationRequest = LocationRequest.create();
        // Establece el intervalo de actualizaci�n
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);
        // Usa prioritariamente la mayor precision
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Establece el intervalo limite en un minuto
        mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
	} 
    
	/**
	 * Verif�ca que Google Play services est� disponible antes de hacer una petici�n.     
     * @return true si Google Play services est� disponible, de otra manera false
     */
    private boolean servicesConnected() {

        //  Verif�ca que Google Play services est� disponible
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.mParentactivity);

        // Si Google Play services est� disponible
        if (ConnectionResult.SUCCESS == resultCode) {
            // En modo debug, registra el estado
            Log.d(LocationUtils.APPTAG, mParentactivity.getString(R.string.play_services_available));
            // Continua
            return true;
            
        // Google Play services no estaba disponible por alguna raz�n
        } else {
            // Muestra un di�logo de error
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this.mParentactivity, 0);
            if (dialog != null) {
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(dialog);
                
                errorFragment.show(this.mParentactivity.getSupportFragmentManager(), LocationUtils.APPTAG);
                
            }
            return false;
        }
    }
    
    /**
     * Show a dialog returned by Google Play services for the
     * connection error code
     *
     * @param errorCode An error code returned from onConnectionFailed
     */
    private void showErrorDialog(int errorCode) {

        // Get the error dialog from Google Play services
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
            errorCode,
            this.mParentactivity,
            LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {

            // Create a new DialogFragment in which to show the error dialog
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();

            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            errorFragment.show(this.mParentactivity.getSupportFragmentManager(), LocationUtils.APPTAG);
        }
    }
    
    
    //***********Public Interface LocationService***************//
    
    /**
     * Connect the client.
     * Wrapper para {@link com.google.android.gms.location.LocationClient.connect()}
     */
    public void connect(){
    	mGoogleApiClient.connect();
    }
    
    /**
     * disconnect the client.
     * Wrapper para {@link com.google.android.gms.location.LocationClient.disconnect()}
     */
    public void disconnect(){
    	mGoogleApiClient.disconnect();
    }
    
    /**
	 * Env�a una petici�n a  los servicios de localizacion para
	 * activar las actualizacions periodicas 
	 */
    public void startPeriodicUpdates(LocationListener lis) {
    	//Log.d(TAG,TAG+" -->startPeriodicUpdates():"+mLocationClient.isConnected());
    	if (mGoogleApiClient.isConnected())//si lla esta conectado hacemos la peticion
    		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, lis);    	
    	else{ //si aun no conecto, la petici�n se realiza en el onConnect cuando regrese del call, 
    		//(dejamos que se haga asincronemente)
    		mRequesStartUpdates =  true;
    		mLocationListener = lis;
    	}
    }

	/**
	 * Env�a una petici�n a  los servicios de localizacion para
	 * parar las actualizacions periodicas 
	 */
    public void stopPeriodicUpdates(LocationListener lis) {
    	if (mGoogleApiClient.isConnected())    		
    		LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, lis);

    }
    
    
    /**
     * Invocado cuando se quiere obtener la geolocalizacion.
     * @return Devuelve un objeto de tipo Location con las coordenadas o null en caso de fallo
     */	    
    public Location getLocation() {
        // Si los servicios de Google Play est�n disponibles
        if (servicesConnected()) {
            //Obtenemos la localizaci�n actual 
            return LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);	            
        }
        return null;
    }
    
    /**
     * Invocado cuando se quiere obtener las coordenadas de geolocalizacion.
     * @return Devuelve un string con la latitud y longitud o en caso de fallo un string vacio
     */
    public String getLocationString() {
    	Location currentLocation=null; 
        // Si los servicios de Google Play est�n disponibles
        if (servicesConnected()) 
            //Obtenemos la localizaci�n actual
        	currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);	           
        
        // Trasnformamos el objeto localizaci�n 
        return LocationUtils.getLatLng(this.mParentactivity, currentLocation);
    }
    
    /**
     * Invocado cuando se quiere obtener las coordenadas de geolocalizacion.
     * @return Devuelve un string con la latitud o en caso de fallo un string vacio
     */
    public String getLocationLatitude() {	    		        
        // Trasnformamos el objeto localizaci�n 
        return LocationUtils.getLat(this.mParentactivity, this.getLocation());
    }
    
    /**
     * Invocado cuando se quiere obtener las coordenadas de geolocalizacion.
     * @return Devuelve un string con la longitud o en caso de fallo un string vacio
     */
    public String getLocationLongitude() {	        
        // Trasnformamos el objeto localizaci�n 
        return LocationUtils.getLng(this.mParentactivity, this.getLocation());
    }

    /**
     * Invoked by the "Get Address" button.
     * Get the address of the current location, using reverse geocoding. This only works if
     * a geocoding service is available.
     *
     * @param v The view object associated with this method, in this case a Button.
     */	 
    public void getAddress(ProgressBar progressbar, AddressNotifyObserver addressOb) {
        // En Gingerbread y versiones superiores, usar Geocoder.isPresent() para comprobar si el geocoder esta disponible.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && !Geocoder.isPresent()) {
        	//No hay geocoder. Emitir un mensaje de error.
            Toast.makeText(this.mParentactivity, R.string.no_geocoder_available, Toast.LENGTH_LONG).show();
            return;
        }

        if (servicesConnected()) {
            // Obtenemos la localizaci�n actual 
            Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        
            // Start the background task
            (new LocationGeocoderClient.GetAddressTask(this.mParentactivity,progressbar,addressOb)).execute(currentLocation);
        }
    }
	    
	    
    /**
     * Una AsyncTask que llama a getFromLocation() en el background.
     * La clase usa los siguientes tipos genericos:
     * Location - Un {@link android.location.Location} objeto que contiene la actual localizaci�n,
     *            pasada como par�metro de entrada a doInBackground()
     * Void     - indica que no se usan unidades de progreso en esta subclase
     * String   - Una direcci�n pasada a onPostExecute()
     * @author Domin
     */	    
    protected class GetAddressTask extends AsyncTask<Location, Void, String> {
    	
        Context localContext;
        ProgressBar progressbar;	        
        AddressNotifyObserver addressOb;
        /**
         * Constructor llamado por el sistema para instanciar la tarea
         * @param context contexto de la aplicacion 
         * @param progressbar barra de progreso indeterminada de la UI que se activar� y se desactivar� mientras dure la tarea, 
         * si se pasa null como argumento no se activar� ninguna 
         * @param addressOb instancia al la que se notificar� la direcci�n una vez que se obtenga
         */
        public GetAddressTask(Context context, ProgressBar progressbar, AddressNotifyObserver addressOb) {

        	//Requerido por la semantica del AsyncTask
            super();
            //Seleccionamos un contexto para la tarea en segundo plano
            localContext = context;
            //Selecionamos una barra de progreso mientras la tarea esta en curso 
            this.progressbar = progressbar;
            //Selecianamos el observador al que se le notificara la dirrecion obtenida
            this.addressOb = addressOb;
        }
        
        @Override
        protected void onPreExecute() {	        
        	super.onPreExecute();
        	if (progressbar!=null){
	            // Ponemos visible el progress bar de haberlo
        		progressbar.setVisibility(View.VISIBLE);
    			//encendemos el progress bar
    			progressbar.setIndeterminate(true);	        	
        	}
        }
        
        /**
         * Obtiene una instancia del servicio de geocodificaci�n, 
         * pasa la latitud y longitud a ella, formatea la direcci�n devuelta, 
         * y devuelve la direcci�n al hilo de la interfaz de usuario.
         */	        
        @Override
        protected String doInBackground(Location... params) {	            
        	/*
        	 * Obtiene una nueva instancia de servicio de geolocalizacion, lo establece para localizar
        	 * direcciones. Este ejemplo usa android.location.Geocoder, pero otros geocoders que cumplan con los
        	 * est�ndares de direcci�n tambi�n pueden ser usados.   
        	 */
            Geocoder geocoder = new Geocoder(localContext, Locale.getDefault());

            // Get the current location from the input parameter list
            Location location = params[0];

            // Create a list to contain the result address
            //Crea una lista que contiene las direcciones resultado
            List <Address> addresses = null;

            //Intenta obtener la direccion de la localizacion actual. Captura los problemas IO o de la red.
            try {
                
            	/*
            	 * Llama al metodo sincrono getFromLocation() con la latitud y longitud 
            	 * de la localizacion temporal. Devuelve a lo sumo  1 direcci�n.
            	 */
                addresses = geocoder.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1
                );                

                // Catch network or other I/O problems.
                } catch (IOException exception1) {

                    // Log an error and return an error message
                    Log.e(LocationUtils.APPTAG, localContext.getString(R.string.IO_Exception_getFromLocation));

                    // print the stack trace
                    exception1.printStackTrace();

                    // Return an error message
                    return (localContext.getString(R.string.IO_Exception_getFromLocation));

                // Catch incorrect latitude or longitude values
                } catch (IllegalArgumentException exception2) {

                    // Construct a message containing the invalid arguments
                    String errorString = localContext.getString(
                            R.string.illegal_argument_exception,
                            location.getLatitude(),
                            location.getLongitude()
                    );
                    // Log the error and print the stack trace
                    Log.e(LocationUtils.APPTAG, errorString);
                    exception2.printStackTrace();
                    
                    return errorString;
                }
            
                // If the reverse geocode returned an address
                if (addresses != null && addresses.size() > 0) {

                	//Obtenemos la primera direcci�n
                    Address address = addresses.get(0);

                    //Formatea la primera linea de la dirreci�n
                    String addressText = localContext.getString(R.string.address_output_string,

                    		//Si hay un nombre de carretera, lo a�adimos
                            address.getMaxAddressLineIndex() > 0 ?
                                    address.getAddressLine(0) : "",

                            //La localidad es normalemente una ciudad
                            address.getLocality(),

                            //El pa�s de la direcci�n
                            address.getCountryName()
                    );
                    
                    // Devuelve la direcci�n como string
                    return addressText;

                // Si no hay ninguna direcci�n, mostramos un mensaje
                } else {
                  return localContext.getString(R.string.no_address_found);
                }
        }
	        
        /**
         * Metodo llamado una vez doInBackground() se completa. Establece el texto de
         * un UI elemento que muestra la direcci�n. Este m�todo corre en el thread del UI.
         */
        @Override
        protected void onPostExecute(String address) {
        	
        	if (progressbar!=null){
	            //Ponemos invisible el progress bar de haberlo
    			//progressbar.setVisibility(View.INVISIBLE);;
    			//Apagamos el progress bar
        		progressbar.setIndeterminate(false);
        	}
         
            //Notificamos la dirreci�n nueva obtenida
        	this.addressOb.addressNotify(address);	            
        }
    }	   
}

	