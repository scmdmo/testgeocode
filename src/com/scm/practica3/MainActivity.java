package com.scm.practica3;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ShareActionProvider;

import com.google.android.gms.maps.model.LatLng;
import com.scm.lib.location.LocationGeocoderClient;
import com.scm.lib.location.LocationGeocoderClient.AddressNotifyObserver;
import com.scm.lib.location.res.LocationUtils;
import com.scm.practica3.dialogs.GetAddressDialogFragment.GetAddressDialogListener;
import com.scm.practica3.dialogs.MapTriggerDialogFragment;
import com.scm.practica3.dialogs.MapTriggerDialogFragment.MapTriggerDialogListener;
import com.scm.practica3.fragments.Geolocation;
import com.scm.practica3.fragments.InverseGeolocation;

public class MainActivity extends FragmentActivity implements 
		MapTriggerDialogListener, 
		GetAddressDialogListener, 
		//OnSharedPreferenceChangeListener, 
		InverseGeolocation.InverseGeolocationCallbacks,
		Geolocation.GeolocationCallbacks  {
	
	private static final String TAG = "MainActivity";
	
	/***********keys**********/
	String GEO_KEY;
	String UPDATE_KEY;
	String SWIPE_KEY;	
	
	private MainPagerAdapter mainPagerAdapter;		
	private CustomViewPager mViewPager;
	private ActionBar actionBar;
	private SharedPreferences sharedPref;
	private LocationGeocoderClient mLocationClient;
	
	private boolean mPeriodicUpdates;
	private String mAddressGeolocation;
	
	private int mMethod;
	
	private ShareActionProvider mShareActionProvider;
	//MenuItem del EasyShare
	private MenuItem mEasyShare;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG,TAG+" onCreate()");
		setContentView(R.layout.activity_main);

		//cargamos las key de las preferencias
		GEO_KEY = getString(R.string.pref_key_geo_method);
		UPDATE_KEY = getString(R.string.pref_key_updates);
		SWIPE_KEY = getString(R.string.pref_key_swipe);			
		
		//Creamos un view pager personalizado, le adjuntamos un adaptador y selecionamos un listener para el adaptador
		this.mainPagerAdapter=new MainPagerAdapter(getSupportFragmentManager(), getApplicationContext());
		this.mViewPager=(CustomViewPager) findViewById(R.id.pager);
		this.mViewPager.setAdapter(this.mainPagerAdapter);	
		
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});
		
		actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(false);
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
	    for (int i=0; i < this.mainPagerAdapter.getCount(); i++){
	    	ActionBar.Tab tab = actionBar.newTab();
	    	tab.setText(this.mainPagerAdapter.getPageTitle(i));
	    	tab.setTabListener(new TabListener(){
				@Override
				public void onTabReselected(Tab tab, FragmentTransaction ft) {}

				@Override
				public void onTabSelected(Tab tab, FragmentTransaction ft) {
					mViewPager.setCurrentItem(tab.getPosition());
				}

				@Override
				public void onTabUnselected(Tab tab, FragmentTransaction ft) {}	    		
	    	});
	    	actionBar.addTab(tab);
	    }	    
	}
	
	@Override
	protected void onRestart() {
		Log.d(TAG,TAG+" onRestart()");
		super.onRestart();
	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		Log.d(TAG,TAG+" onStart()");
		
		sharedPref=PreferenceManager.getDefaultSharedPreferences(this);
		//Actualizamos las preferencias en el arranque de la activity
		mViewPager.setPagerEnable(sharedPref.getBoolean(SWIPE_KEY,true));
		
		//no se puede usar aqui por culpa del pager adapter
		//no se puede recuperar el fragment puesto que si lo intentamos recuperar
		//en el onStart(), el pager adapter aun no creo los fragments y tenemos una referencia nula.
		//si intentamos forzar la creacion del fragment en el adapter usando el getItem(0), nos encontramos
		//con el fallo de que aun no se a ejecutado ligado el fragment a la activity
		//la unica solucion que veo facilmente es ejecutar la actualizaci�n en el onResume()-> tampoco va
//		mainPagerAdapter.getInversegeolocationFragment().setUpdateIndicatorState(
//				sharedPref.getBoolean(UPDATE_KEY, false));
		mPeriodicUpdates = sharedPref.getBoolean(UPDATE_KEY, false);
		
		mMethod=Integer.parseInt(sharedPref.getString(GEO_KEY, "-1"));
		switch (mMethod) {
			case 1://http request
				Log.d(TAG,"Load location method 'http request' from shared preferences value:"+mMethod);				
				break;	
			default://geocoder
				Log.d(TAG,"Load location method 'geocoder' from shared preferences value:"+mMethod);				
				break;
		}
		
		mLocationClient = new LocationGeocoderClient(this);
		mLocationClient.connect();
		
		//TODO a�adir las restantes preferencias al arranque 
		
	}
		
	//TODO:Por el momento no cargamos los estados guardados, asi cada vez que se 
	//inicializa la aplicacion no comprueba estados viejos guardados (cambio de orientacion)	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		
		//super.onRestoreInstanceState(savedInstanceState);
	}	
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG,TAG+" onResume()");
		
		//sharedPref.registerOnSharedPreferenceChangeListener(this);//FIXME: Creo que sobra, el on start ya lee las preferences
	}
	
	//TODO:Por el momento no salvamos los estados, asi cada vez que se 
	//inicializa la aplicacion no comprueba estados viejos guardados (cambio de orientacion)	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		
		//super.onSaveInstanceState(outState);
	}	
	
	@Override
	protected void onPause() {
		Log.d(TAG,TAG+" onPause()");		
		//sharedPref.unregisterOnSharedPreferenceChangeListener(this);//FIXME: Creo que sobra, el on start ya lee las preferences
		super.onPause();		
	}
	
	@Override
	protected void onStop() {
		Log.d(TAG,TAG+" onStop()");
		mLocationClient.disconnect(); 
//		if (mEasyShare!=null)
//			mEasyShare.setVisible(false);		
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {
		Log.d(TAG,TAG+" onDestroy()");
		super.onDestroy();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {	
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
			case LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST:
				mLocationClient.onActivityResult(requestCode, resultCode, data);
				break;
	
			default:
				break;
		}
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		// Locate MenuItem with ShareActionProvider
	    mEasyShare = menu.findItem(R.id.menu_item_share);
	    mEasyShare.setVisible(false);	    
	    // Fetch and store ShareActionProvider
	    mShareActionProvider = (ShareActionProvider) mEasyShare.getActionProvider();	    

	    // Return true to display menu
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			// Lanzamos la activity de settings			
	        startActivity(new Intent(this,SettingsActivity.class));			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// TODO: seguir arreglando esto
	private void launchGoogleMapActivity(double latitude, double longitude, String address) {    	
    	
    	String uriBegin = "geo:" + latitude + "," + longitude;
    	String query = latitude + "," + longitude + "(" + address + ")";
    	String encodedQuery = Uri.encode(query);
    	String uriString = uriBegin + "?q=" + encodedQuery + "&z=12";
 	
    	Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(uriString));	        
//	        if (intent.resolveActivity(getPackageManager()) != null)
	        	startActivity(intent);	        
    	//}
    }
	
	// Call to update the share intent
	private void setShareIntent(Intent shareIntent) {
	    if (mShareActionProvider != null) {
	        mShareActionProvider.setShareIntent(shareIntent);	        
        }
    }
	
	
	//********************SharedPreference Callback**********************//
	//NO USADO, se usa como alternativa la carga/recarga en el onStart de los estados de las variables
//	@Override
//	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {		
//		Log.d(TAG,"onSharedPreferenceChanged");
//		if (key.equals(GEO_KEY)){//metodo de geolocalizacion
//			mMethod=sharedPreferences.getInt(GEO_KEY, 1);
//			switch (mMethod) {
//				case 1://http request
//					Log.d(TAG,"Change location method 'http request' from shared preferences");					
//					break;
//		
//				default://geocoder
//					Log.d(TAG,"Change location method 'geocoder' from shared preferences");					
//					break;
//			}
//			//mLocationClient = new LocationGeocoderClient(this);
//		}
//		
//		if (key.equals(UPDATE_KEY)){//actualizaciones automaticas desactivas por defecto
//			//manejar las actualizaciones automaticas
//			mPeriodicUpdates = sharedPref.getBoolean(UPDATE_KEY, false);			
//		}
//		
//		if (key.equals(SWIPE_KEY))//swipe activo por defecto
//			mViewPager.setPagerEnable(sharedPreferences
//					.getBoolean(SWIPE_KEY,true));		
//	}	
	
	
	//*************************Dialogs Callbacks*************************//
	
	@Override
	public void onMapTriggerDialogPositiveClick(Bundle bundle) {
		Log.d("Dialog MapTrigger", "positive click");
		LatLng latlng = (LatLng) bundle.get(MapTriggerDialogFragment.LATLNG);
		String address = bundle.getString(MapTriggerDialogFragment.MAP_LABEL);
		this.launchGoogleMapActivity(latlng.latitude, latlng.longitude, address);
	}
	
	@Override
	public void onDialogGetAddresClick(DialogFragment dialogfragment, Geolocation parentdialogFragment) {
		Dialog dialog = dialogfragment.getDialog();
		EditText edittext=(EditText)dialog.findViewById(R.id.dialogAddressEditText);		
		mAddressGeolocation=edittext.getText().toString();//TODO: revisar lo referente al mAddressGeolocation		
		parentdialogFragment.setAddress(edittext.getText().toString());
		//mainPagerAdapter.getGeolocationFragment().setAddress(address);		
	}	
	
	
	//*******************InverseGeolocation Callbacks********************//
	
	@Override
	public LocationGeocoderClient getLocationGeocoderClient() {
		return mLocationClient;
	}
	
	@Override
	public boolean getPeriodicUpdatesState() {		
		return this.mPeriodicUpdates;
	}	
		
	@Override
	public void getAddressAsync(ProgressBar progressbar, AddressNotifyObserver addressOb) {
		mLocationClient.getAddress(progressbar, addressOb);
		
	}
	
	@Override
	public int getGeolocationMethod() {		
		return mMethod;
	}
	
	public void easyShareAddress(String addr){
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, addr);
		sendIntent.setType("text/plain");
		mEasyShare.setVisible(true);		
		this.setShareIntent(sendIntent);		
	}
	
	
	//**********************Geolocation Callbacks************************//	
	
	@Override
	public String getAddressGeolocation() {		
		return this.mAddressGeolocation;
	}
	

	//*************************Nested Classes*************************//
	/**
	 * Esta clase permite navegar con swipe entre los distintos fragments.
	 */	
	public static class MainPagerAdapter extends FragmentPagerAdapter {
		
		final int totalsections; 
		final String[] tabTitles;
		
		public MainPagerAdapter(FragmentManager fm, Context cntx) {
	        super(fm);
	        tabTitles=cntx.getResources().getStringArray(R.array.tab_titles);	        
	        totalsections=tabTitles.length;
	    }

	    @Override
	    public android.support.v4.app.Fragment getItem(int i) {	    	
			switch (i) {
			 	case 0:
	            	return new InverseGeolocation();	                
	            default:
	            	return new Geolocation();	            	
	        }
	    }

	    @Override
	    public int getCount() {
	        return totalsections;
	    }

	    @Override
	    public CharSequence getPageTitle(int position) {	    	
	        return this.tabTitles[position];	    	
	    }	    
	}
}
