package com.scm.practica3;

import android.app.Activity;
import android.os.Bundle;

import com.scm.practica3.preferences.Settings;

public class SettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
            .replace(android.R.id.content, new Settings()).commit();
		}
	}
}
