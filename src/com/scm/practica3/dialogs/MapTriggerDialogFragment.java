package com.scm.practica3.dialogs;

import com.scm.practica3.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class MapTriggerDialogFragment extends DialogFragment {
	public static final String LATLNG = "LatLng";
	public static final String MAP_LABEL = "Map_label";
	public static final String MAP_ZOOM="map_zoom";
	
	private Bundle mArguments;

    private MapTriggerDialogListener activityListener;
	
	public interface MapTriggerDialogListener {
		/**
		 * 
		 * @param meta datos para lanzar el google maps
		 */
        public void onMapTriggerDialogPositiveClick(Bundle bundle);        
    }    
	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {          
            activityListener = (MapTriggerDialogListener) activity;
        } catch (ClassCastException e) {            
            throw new ClassCastException(activity.toString()
                    + " must implement MapTriggerDialogListener");
        }
    }
	
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
		//Recuperamos los argumentos asociados al dialog
		mArguments = getArguments();		
		
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.diag_mapTrigger_title);
        builder.setMessage(R.string.diag_mapTrigger_message);
        builder.setPositiveButton(R.string.diag_mapTrigger_buton_ok, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   activityListener.onMapTriggerDialogPositiveClick(mArguments);
                   }
        });
        //no necesitamos hacer nada para el boton cancelar
        builder.setNegativeButton(R.string.diag_mapTrigger_buton_cancel, null);
        
        //XXX: borrar esto si no fallos una vez probado
//        builder.setNegativeButton(R.string.diag_mapTrigger_buton_cancel, new DialogInterface.OnClickListener() {
//                   public void onClick(DialogInterface dialog, int id) {
//                       // User cancelled the dialog
//                   }
//        });
        
        return builder.create();
    }
}
