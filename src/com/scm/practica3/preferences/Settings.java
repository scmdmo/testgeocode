package com.scm.practica3.preferences;

import com.scm.practica3.R;
import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class Settings extends PreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		
		//Cargamos las preferencias de los recursos XML
		addPreferencesFromResource(R.xml.settings);
	}
	
}
