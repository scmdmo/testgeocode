package com.scm.practica3;

import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.scm.lib.location.Place;

public class FunctionsJSON {
	
	public static JSONObject getJSONObjectFromUrl(String url){
    	
    	HttpGet httpGet = new HttpGet(url);
    	HttpClient client = new DefaultHttpClient();
    	HttpResponse response;
    	StringBuilder stringBuilder = new StringBuilder();
    	JSONObject jsonObject = null;
    	
    	try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			
			// Force utf-8 codification
			InputStreamReader stream = 
					new InputStreamReader(entity.getContent(), "utf-8");
			
			int b;
			while((b = stream.read()) != -1) {
				stringBuilder.append((char)b);
			}
			
			jsonObject = new JSONObject(stringBuilder.toString());
			
		} catch ( JSONException | IOException e) {			
			e.printStackTrace();
		}    	
		return jsonObject;
    }
	
	public static Place getPlaceFromJSONObject(JSONObject jsonObject){
		
    	float lng = 0;
    	float lat = 0;
    	String namePlace = null;    	
    	try {    		
			namePlace = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
					.getString("formatted_address");
			
			lng = (float) ((JSONArray) jsonObject.get("results")).getJSONObject(0)
					.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
			
			lat = (float) ((JSONArray) jsonObject.get("results")).getJSONObject(0)
					.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
    	
    	} catch (JSONException e) {			
			e.printStackTrace();
		}
    	return new Place(lat, lng, namePlace);   	    	
    }
	
}
