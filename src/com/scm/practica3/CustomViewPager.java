package com.scm.practica3;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager{	

	private boolean pagerEnable=true;
	
	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);		
	}

	public CustomViewPager(Context context) {
		super(context);
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (this.pagerEnable)
			return super.onInterceptTouchEvent(ev);
		else
			return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (this.pagerEnable)
			return super.onTouchEvent(ev);
		else
			return false;
	}	
		
	public boolean isPagerEnable(){
		return this.pagerEnable;
	}
	
	public void setPagerEnable(boolean state){
		this.pagerEnable=state;
	}	
}
