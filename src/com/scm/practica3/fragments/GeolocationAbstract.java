package com.scm.practica3.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.scm.practica3.R;

public abstract class GeolocationAbstract extends Fragment {
	
	//private static final String TAG = "GeolocationAbstract";
	
	protected boolean checkNetwork(){		
		//Check internet connection
		Context context = getActivity().getApplicationContext();
	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);     
	    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
	    if (activeNetwork == null || !activeNetwork.isConnected()){
	    	if (activeNetwork != null && activeNetwork.isConnectedOrConnecting())//esta conectando
	    		Toast.makeText(context,
	    					 	getString(R.string.connecting),	    					 	
	    					 	Toast.LENGTH_LONG).show();	
	    	else //no esta conectada ni esta conectando
	    		Toast.makeText(context,
					 	getString(R.string.no_connection), 
					 	Toast.LENGTH_LONG).show();	    		
	    	return false;
	    }
	    return true;
	}	
}
